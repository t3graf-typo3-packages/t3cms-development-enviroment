# TYPO3 Extension: `My Ext`
[![Latest Stable Version](https://img.shields.io/packagist/v/vendor/my-ext?label=stable&color=33a2d8&logo=packagist&logoColor=white)](https://packagist.org/packages/vendor/my-ext)
[![TYPO3 compatibility](https://img.shields.io/static/v1?label=TYPO3&message=v10%20%7C%20v11&color=f49800&&logo=typo3&logoColor=f49800)](https://typo3.org)
[![Packagist PHP Version](https://img.shields.io/packagist/dependency-v/t3graf/extended-bootstrap-package/php?color=purple&logo=php&logoColor=white)]()
[![Total Downloads](https://img.shields.io/packagist/dt/vendor/my-ext?color=1081c1&logo=packagist&logoColor=white)](https://packagist.org/packages/vendor/my-ext)
[![License](https://img.shields.io/packagist/l/vendor/my-ext?color=498e7f)](https://gitlab.com/typo3graf/developer-team/extensions/extended_bootstrap_package/-/blob/master/LICENSE.md)
[![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline/t3graf-extensions/extended_bootstrap_package/master?label=CI%2FCD&logo=gitlab)](https://gitlab.com/t3graf-extensions/extended_bootstrap_package/pipelines)

> Extension description here.

## Features


* Feature list

* [Well documented](https://docs.typo3.org/typo3cms/extensions/)

## Compatibility

| Website Toolbox     | TYPO3     | PHP       | Support / Development                |
|----------|-----------|-----------|--------------------------------------|
| dev-main | 10 - 11   | 7.4 - 8.1 | unstable development branch          |
| 1.x        | 10 - 11   | 7.4 - 8.1 | features, bugfixes, security updates |


## Installation

### Installation using composer

The recommended way to install the extension is by using [Composer](https://getcomposer.org).

`composer require t3graf/my-ext`.

### Installation from TYPO3 Extension Repository (TER)

Download and install the extension with the extension manager module.

## Minimal setup

1) Include the static TypoScript of the extension. You don't need to include the TypoScript of Bootstrap package.

<!--## 4. Administration

### Create content element

## 5. Configuration

### Extension settings

### Constants
-->
## Need help?

* Read how to install, configure and use mask in the [official documentation](https://docs.typo3.org/p/mask/mask/main/en-us/)
* Join the "#ext-mask" channel on [TYPO3 Slack](https://typo3.slack.com/archives/C0FD5F6P2) and ask the mask community.
* [Visit our website](https://mask.webprofil.at) to find more information about mask

## Found a bug?

* First check out the main branch and verify that the issue is not yet solved
* Have a look at the existing [issues](https://github.com/gernott/mask/issues/), to prevent duplicates
* If not found, report the bug in our [issue tracker](https://github.com/gernott/mask/issues/new/)

**Please use Gitlab only for bug-reports or feature-requests.**

## Like a new feature?

* Have a look at our [project page](https://github.com/Gernott/mask/projects/1)
* If your idea is not listed here, get in [contact](https://mask.webprofil.at/imprint/) with us
* If you want to sponsor a feature, get in [contact](https://mask.webprofil.at/imprint/) with us
* If you want to develop a feature, get in [contact](https://mask.webprofil.at/imprint/) to plan a strategy


## Credits

This extension was created by Mike Tölle in 2021 for [T3graf media-agentur, Recklinghausen](https://www.t3graf-media.de).

Find examples, use cases and best practices for this extension in our [extended_bootstrap_package blog series on t3graf.de](https://www.t3graf-media.de/blog/).

[Find more TYPO3 extensions we have developed](https://www.t3graf-media.de/) that help us deliver value in client projects. As part of the way we work, we focus on testing and best practices to ensure long-term performance, reliability, and results in all our code.


## Links
|                  | URL                                            |
|------------------|------------------------------------------------|
| **Repository:**  | LINK     |
| **Documentation:** | LINK |
| **TER:**         | [extensions.typo3.org/](https://extensions.typo3.org/)    |

- **Demo:** [www.t3graf-media.de/typo3-extensions/extended-bootstrap-package](https://www.t3graf-media.de/typo3-extensions/extended-bootstrap-package)
- **Found an issue?:** [gitlab.com/t3graf-extensions/extended_bootstrap_package/issues](https://gitlab.com/t3graf-extensions/extended_bootstrap_package/issues)
